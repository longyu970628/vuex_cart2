import Vue from 'vue'
import Router from 'vue-router'
import Index from '../components/index'
import Classify from '../components/classify'
import Find from '../components/find'
import Cart from '../components/cart'
import Mine from '../components/mine'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect:'/classify'
    },
    {
      path: '/index',
      name: 'index',
      component: Index
    },
    {
      path: '/classify',
      name: 'classify',
      component: Classify
    },
    {
      path: '/find',
      name: 'find',
      component: Find
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart
    },
    {
      path: '/mine',
      name: 'mine',
      component: Mine
    }
  ]
})
