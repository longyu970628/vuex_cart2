import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state:{
        list:[
            {
                title:'热卖',
                child:[
                    {
                        url:'https://fms-image.missfresh.cn/cbbdf56d5d954022bc679ee7ce511acd.jpg?iopcmd=convert&dst=webp',
                        name:'【新人】南丰蜜桔500g',
                        info:'吃过都知道 一口蜜桔满嘴甜',
                        price:2.9,
                        count:0
                    },
                    {
                        url:'https://fms-image.missfresh.cn/a44b6b6f6c2042d5b541cf06ec5d7d7d.jpg?iopcmd=convert&dst=webp',
                        name:'【新人】泰国龙眼500g*1盒',
                        info:'珍珠级龙眼 吃出3D肉感',
                        price:4.8,
                        count:0
                    },
                    {
                        url:'https://fms-image.missfresh.cn/074b7e6706ba4a0faebfa7101cdf28ae.jpg?iopcmd=convert&dst=webp',
                        name:'【新人】越南红肉火龙果320g起',
                        info:'一手能掌握的红肉心',
                        price:8.8,
                        count:0
                    }
                ]
            },
            {
                title:'水果',
                child:[
                    {
                        url:'https://fms-image.missfresh.cn/3a8b66cb3f0845fd892d4ae4f7554449.jpg?iopcmd=convert&dst=webp',
                        name:'库尔勒香梨5个475g起',
                        info:'落地即碎的多汁脆梨',
                        price:12.9,
                        count:0
                    },
                    {
                        url:'https://fms-image.missfresh.cn/bee60dd79b4244ccab9d265a566cbfa0.jpg?iopcmd=convert&dst=webp',
                        name:'广西百香果5个250g起',
                        info:'仿佛能尝到一百种果香味儿',
                        price:4.8,
                        count:0
                    },
                    {
                        url:'https://fms-image.missfresh.cn/c005a9d820ef4077a7fb3a95fde21b31.jpg?iopcmd=convert&dst=webp',
                        name:'越南白火龙果430g起',
                        info:'从越南那边 一路清爽到你嘴边',
                        price:9.9,
                        count:0
                    }
                ]
            }
        ],
        num:0,
        carList:[]
    },
    mutations: {
        changeCount(state,data) {
            state.list[data.index].child[data.ind].count++;
            state.num++;
            let arr = [];
            state.list.forEach((item,index) => {
                item.child.filter((v,i) => {
                    v.count > 0 ? arr.push(v) : ''
                })
            })
            state.carList = arr;
        },
        changeReduce(state,data) {
            state.list[data.index].child[data.ind].count--;
            state.num--;
            let arr = [];
            state.list.forEach((item,index) => {
                item.child.filter((v,i) => {
                    v.count > 0 ? arr.push(v) : ''
                })
            })
            state.carList = arr;
        },
        changeCarlistAdd(state,data) {
            state.carList[data.index].count++;
            state.num++;
            let arr = [];
            state.list.forEach((item,index) => {
                item.child.filter((v,i) => {
                    v.count > 0 ? arr.push(v) : ''
                })
            })
            state.carList = arr;
        },
        changeCarlistReduce(state,data) {
            state.carList[data.index].count--;
            state.num--;
            let arr = [];
            state.list.forEach((item,index) => {
                item.child.filter((v,i) => {
                    v.count > 0 ? arr.push(v) : ''
                })
            })
            state.carList = arr;
        }
    }
})